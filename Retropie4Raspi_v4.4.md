# Retropie 4.4 树莓派中文使用说明



## 相关名词

### Retropie

> Retropie可以将你的树莓派或者PC变成一台复古游戏机。 Retropie基于完整的操作系统之上，你可以把它装在Raspbian系统上，或者直接刷入完整的镜像然后再添加更多的软件。

### 模拟器

> 一种能使电脑运行其他平台软件的软件。Retropie的SD镜像预装了很多不同的模拟器，可以让你玩很多不同平台的游戏，比如FC，SFC，MD，CPS1，CPS2，NEOGEO，PS1等等。

### ROM

> ROM是电子版的游戏卡带。在模拟器中加载ROM相当于在实体游戏机中插入卡带。 **ROM是涉及相关版权的，所以不会被集成再RETROPIE中，玩家们需要自己去GOOGLE或者百度找找啦**。

### BIOS

> BIOS是一种辅助硬件工作的小软件。很多模拟器不需要BIOS，但是有一部分（或者说相当一部分）游戏需要你提供BIOS文件。如果在玩游戏的时候需要BIOS，模拟器的页面会提示你的（不过经过测试好像并没有，所以建议玩家最好把BIOS装全）。 **BIOS也是涉及相关版权的，需要玩家自己寻找**。

## 首次安装

### 需要用到的硬件

- 一台树莓派，任何型号都可以，如果需要更好的表现建议使用3B+
- 树莓派外壳，可以保护电路，也更美观，不是必要的~
- TF卡，建议使用class 10以上规格的，容量取决于你自己，如果需要玩PS1或者MAME，建议准备32G以上的，笔者装了4000多MAME ROMS和十几个PS1镜像，用了大概25G的空间。
- TF读卡器，必需品，无须多说
- HDMI线，用于连接显示器
- 一台有HDMI接口的显示器
- 无线局域网
- 5V 2A的Micro USB接口的电源，可以是充电器，或者是同等规格的充电宝
- USB接口的键盘和鼠标，鼠标其实可以不需要，键盘是必备的
- USB接口的手柄

### 下载镜像

>Retropie for Raspberrypi[固定更新地址](https://github.com/RetroPie/RetroPie-Setup/releases/ "Retropie For Raspberry 下载链接")

- [Retropie4.4 for Raspberrypi 0/1 直链镜像](https://github.com/RetroPie/RetroPie-Setup/releases/download/4.4/retropie-4.4-rpi1_zero.img.gz "Retropie4.4 for Raspberrypi 0/1 直链镜像")
- [Retropie4.4 for Raspberrypi 2/3 直链镜像](https://github.com/RetroPie/RetroPie-Setup/releases/download/4.4/retropie-4.4-rpi2_rpi3.img.gz "Retropie4.4 for Raspberrypi 2/3 直链镜像")

### 解压缩

> 下载完成SD卡镜像之后，你需要把**.gz**文件中的**.img**文件解压到你的电脑中。 你可以选择使用[7-zip](http://www.7-zip.org/ "7-zip")或者[WinRar](http://www.winrar.com.cn/ "WinRar") 

### 向SD卡写入镜像

- 下载[Etcher](https://etcher.io/ "Etcher")或者[Win32DiskImager](http://sourceforge.net/projects/win32diskimager/ "Win32DiskImager") 这是必要的刷入工具
- 将TF卡插入读卡器中，使用工具将镜像刷入卡中

### 配置控制器

> 将TF卡插入Raspberrypi中，连接手柄，键盘，显示器，然后开启电源。首次启动会缓慢一些，Retropie的文件系统会初始化，等待之后会看到如下界面，此时可为所有模拟器配置公用的手柄按键：

<div align=center><img height=300 src="https://cloud.githubusercontent.com/assets/10035308/9140482/cf42f25c-3cee-11e5-8f91-c1fc1c57175c.png"/ ></div>

> 按住键盘或者手柄的任意键，屏幕底部会出现它们的名字，然后再按任意键会进入相应的配置界面：

<div align=center><img height=300 src="https://cloud.githubusercontent.com/assets/10035308/9140505/f5c19e38-3cee-11e5-965e-0e4e85ddaf56.png"/ ></div>

> 根据图示依次按键进行配置，如果你想跳过某个按键配置，长安任意键即可：

<div align=center><img height=300 src="https://cloud.githubusercontent.com/assets/10035308/9140518/0263b9c8-3cef-11e5-922f-42f790f3be91.png"/ ></div>

> 如果你想配置多个控制器，你可以在模拟器的开始菜单进行多次相同的操作。 不同布局的手柄可以根据下图进行参考配置：
> 
> SFC手柄：
<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/22185414/f129dc28-e099-11e6-8524-93facf275eda.png"/ ></div>
> XBOX手柄：
<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/22185415/f12ff342-e099-11e6-8adb-d18e9c638e94.png"/ ></div>
> PS3手柄
<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/22185413/f10f27de-e099-11e6-97a4-ecbbc82c9e46.png"/ ></div>

### 关于热键(Hotkey)的说明

> 热键和其它键位进行组合可以进行一些便捷的操作，比如快速存档，快速独挡，退出到主界面等等。推荐使用**Select**作为热键，在配置控制器最后，系统会提示你进行热键配置。以下是默认配置下热键组合的功能：

- Hotkey + 开始键 : 退出到模拟器界面
- Hotkey + 左肩键 : 快速存档
- Hotkey + 右肩键 : 快速读档
- Hotkey + 左方向 : 切换到上一个存档位置
- Hotkey + 右方向 : 切换到下一个存档位置
- Hotkey + X     : 快速菜单
- Hotkey + B     : 复位

### Emulation Station

> 当你第一次看到下图的时候，你可能会懵圈： 我怎么看不见GBA，SFC或者FC的模拟器呢？ 事情是酱婶儿的，首先你得把ROMS放在系统中特定的目录下，然后重启模拟器系统，你就能看到各种各样的模拟器了，后面会介绍如何导入ROMS：

<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/16217874/c6bbdb3a-3734-11e6-998f-8cc714a320ce.png"/ ></div>

### Wifi

> 如果你希望通过Wifi导入ROMS，或者是进行系统升级，那么你需要设置一下。在主界面按下A键，选择Wifi，再次按下A键

> 选择第一项，连接到Wifi:

<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/16217941/92b08b8c-3735-11e6-8f20-5d1550af0882.png"/ ></div>

> 在扫描列表中选择你的Wifi名称（SSID）:

<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/16217942/92c5a8a0-3735-11e6-86ed-721c1bb81990.png"/ ></div>

> 输入密码：

<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/16217942/92c5a8a0-3735-11e6-86ed-721c1bb81990.png"/ ></div>

> 连接上之后，你会看到你的ip地址和SSID:

<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/16217944/92cb07fa-3735-11e6-9239-66fba394c669.png"/ ></div>

### 安装附加的模拟器 (非必须)

> Retropie 4.0+版本之后，默认不会安装所有的模拟器。安装镜像只会预装那些兼容性和画面表现最好的模拟器, 这应该覆盖了大多数用户的需求。 下面简单说下安装流程： 打开Retropie界面，选择RetroPie-Setup，然后选择Manage Packages，列表中会有很多软件包的名称，你可以看到它们当前的安装状态，稳定版的附加软件在Optional选项中。你可以选择一个然后进行安装或者卸载。

> **重要提醒：** 很多玩家装好Retropie之后，发现很多MAME游戏无法运行，原因有两个，第一，有可能缺少游戏需要的相关的BIOS文件（后文详述）;第二，有可能模拟器版本不支持你导入的ROMS。 第一个原因大家都会意识到，不过很少有人关注第二个原因。Retropie镜像中提供的MAME模拟器仅支持**0.37b5和0.78**两个版本的ROMS，所以有大部分的街机游戏无法正常运行，所以**强烈推荐玩家安装lr-mame2003-plus**，然后使用这个模拟器版本进行游戏，此版本支持0.78-0.188的绝大部分ROMS，目前尚未发现无法运行的街机游戏。

### U盘导入ROMS

> 因为不同国家地区版权的原因，Retropie无法提供游戏ROMS，所以需要玩家自己寻找 :( 。 官方提供了三种方式来导入ROMS，墙裂推荐使用U盘的方式进行导入，方便快捷： 

- U盘必须是FAT32或者NTFS格式
- 在U盘根目录中创建 *retropie* 文件夹
- 将U盘插入Raspberrypi中，等待U盘指示灯停止闪烁，拔下U盘
- 将U盘插回电脑
- 把ROMS文件放在对应模拟器的文件夹下 (上级文件夹为 *retropie/roms*)
- 将U盘插入Raspberrypi中，等待U盘指示灯停止闪烁，这个过程会比较漫长，树莓派的IO量本身比较小，如果一次性搬运很多文件的话，可能需要很久，静静等待吧，指示灯停止闪烁之后就可以拔下U盘了
- 任意界面按下Start键开启菜单，选择Quit，选择Restart Emulationstation重启模拟器
- 然后就可以看到已经分类好的模拟器和游戏了

### 局域网共享导入ROMs

> 这个方法只适用于导入体积较小的ROMS，传递速度可能只有几百kb，视当前网络情况而定，如果你想用这种方式导入PS1游戏的话，那还是算了 :( :

- 打开“我的电脑”，路径中输入Raspberrypi的局域网ip地址:  *\\\192.168.X.X*，或者输入*\\\retropie*按下回车：

<div align=center><img width=600 src="https://cloud.githubusercontent.com/assets/10035308/12865893/d2eab264-cc77-11e5-9ec6-003e13322a5a.png"/ ></div>

- 然后将ROMS文件放在对应模拟器的文件夹下
- 同样的，重启模拟器，刷新游戏菜单

## 相关路径

### BIOS路径

- NEOGEO： */home/pi/RetroPie/roms/neogeo*
- 其它模拟器： */home/pi/RetroPie/BIOS*

### ROMS路径
> 此处只列出MAME模拟器的ROMS路径，因为对应各种版本确实比较迷，其他模拟器都很清晰明了。

|模拟器|ROM路径|支持文件类型|对应的模拟器版本|控制器配置文件路径|
|:--|:--|:--|:--|:--|
|[mame4all-pi](https://retropie.org.uk/docs/MAME/)|arcade or mame-mame4all|.zip|MAME 0.37b5|/opt/retropie/configs/mame-mame4all/cfg/default.cfg|
|[lr-mame2000](https://retropie.org.uk/docs/MAME/)|arcade or mame-libretro|.zip|MAME 0.37b5|/opt/retropie/configs/arcade/retroarch.cfg, or /opt/retropie/configs/mame-mame4all/retroarch.cfg|
|[lr-mame2003](https://retropie.org.uk/docs/MAME/)|arcade or mame-libretro|.zip|MAME 0.78|/opt/retropie/configs/arcade/retroarch.cfg, or /opt/retropie/configs/mame-libretro/retroarch.cfg|
|[lr-mame2003-plus](https://retropie.org.uk/docs/MAME/)|arcade or mame-libretro|.zip|MAME 0.78-MAME 0.188|/opt/retropie/configs/arcade/retroarch.cfg, or /opt/retropie/configs/mame-libretro/retroarch.cfg|
|[lr-mame2010](https://retropie.org.uk/docs/MAME/)|arcade or mame-libretro|.zip or .7z|MAME 0.139|/opt/retropie/configs/arcade/retroarch.cfg, or /opt/retropie/configs/mame-libretro/retroarch.cfg|
|[lr-mame2014](https://retropie.org.uk/docs/MAME/)|arcade or mame-libretro|.zip or .7z|MAME 0.159|/opt/retropie/configs/arcade/retroarch.cfg, or /opt/retropie/configs/mame-libretro/retroarch.cfg|
|[lr-mame2016](https://retropie.org.uk/docs/MAME/)|arcade or mame-libretro|.zip or .7z|MAME 0.174|/opt/retropie/configs/arcade/retroarch.cfg, or /opt/retropie/configs/mame-libretro/retroarch.cfg|
|[AdvanceMAME 0.94](https://retropie.org.uk/docs/MAME/)|arcade or mame-advmame|.zip|MAME 0.94|/opt/retropie/configs/mame-advmame/advmame-0.94.0.rc|
|[AdvanceMAME 1.4](https://retropie.org.uk/docs/MAME/)|arcade or mame-advmame|.zip|MAME 0.106|/opt/retropie/configs/mame-advmame/advmame-1.4.rc|
|[AdvanceMAME 3](https://retropie.org.uk/docs/MAME/)|arcade or mame-advmame|.zip|MAME 0.106|/opt/retropie/configs/mame-advmame/advmame.rc|

## 相关资源

- [MD中文ROMS](https://yadi.sk/d/ZKei06FGao1slg)
- [FC中文ROMS](https://lugiar.ctfile.com/dir/11449240-27299530-05ba1e/)
- [SFC中文ROMS](https://yadi.sk/d/FfoiJ43Wk6MZFA)
- [PS1中文ROMS](https://pan.baidu.com/s/113h-gmXbIFu8RX_NMceD1Q) 密码： vqd5
- [GBA中文ROMS](https://yadi.sk/d/kfAYKs1QJkgEDQ)
- [MAME ROMS 6501个](https://pan.baidu.com/s/13l74Hw-ymALlscnpvrhelg) 密码：uq32
- [Winkawaks ROMS（CPS1/CPS2/NEOGEO 673个）](https://pan.baidu.com/s/1eTSij5o) 密码：yba4



## PLAY!!!!

>至此为止，你已经可以使用Raspberrypi玩各种各样的复古游戏了，enjoy~ 笔者是个初学者，希望能和大家交流 :) ，QQ: 910492367




